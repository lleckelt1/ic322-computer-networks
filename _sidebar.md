- [🏠 Home](/)

- [🪑 Schedule a Meeting](https://calendar.app.google/MrVi9qk7TWpKnAvJ6)

- [📎 Course Policy](/policy/)

- [📏 Assignment Standards](/standards/)

- [🗓️ Class Schedule](/policy/README.md#schedule)

- 📓 Assignments

    - Week 0: Getting Ready
        - [Assignment Overview](/assignments/week00/)
        - [Lab: Create Your Portfolio](/assignments/week00/create-your-portfolio.md)
        - [In class: An Internet Warmup](/assignments/week00/activity-what-is-the-internet.md)
        - [In class: Our Class Rubric](/assignments/week00/activity-class-rubric.md)
    - Week 1: The Internet
        - [Assignment Overview](/assignments/week01/)
        - [Lab: Wireshark Introduction](/assignments/week01/README.md#wireshark-introduction)
    - Week 2: The Application Layer: HTTP
        - [Assignment Overview](/assignments/week02/)
        - [Lab: Build a Server](/assignments/week02/lab-build-a-server.md)
        - [Lab: Build a Multiplayer `telnet` Game](/assignments/week02/lab-telnet-game.md)
    - Week 3: The Application Layer: DNS and Other Protocols
        - [Assignment Overview](/assignments/week03/)
        - [Lab: Register a Domain Name](/assignments/week03/register-a-domain-name.md)
    - Week 4: The Transport Layer: TCP and UDP
        - [Assignment Overview](/assignments/week04/)
        - [Lab: Introduction to Packet Tracer](/assignments/week04/lab-packet-tracer/)

- [🗂️ References](/references/)

- [🪴 Community Material](https://drive.google.com/drive/folders/1OsXHeg7SPHC1k5SgDYtfE9fzGsmNwlB7?usp=sharing)