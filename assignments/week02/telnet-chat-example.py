"""
A simple chat server. 
To run locally, use: `python3 server.py`
To connect as a client, use: `nc <server-ip> 12000`
If running locally, you can connect to the server using netcat: `nc localhost 12000`
"""

from socket import *
import threading

class Connection:
    """
    Holds information about a client connection.
    """
    def __init__(self, socket, address):
        self.socket = socket
        self.address = address



def handle_client(connection):
    """
    Checks for messages from the client (connection). If a message is received, it is broadcasted to all other clients.
    """
    while True:
        message = connection.socket.recv(1024)
        if not message:
            break

        print(f"Message received from {connection.address}: {message.decode()}")

        # Broadcast the message to all other clients
        with lock:
            for otherConnection in sockets:
                if otherConnection != connection:
                    try:
                        otherConnection.socket.send(message)
                    except:
                        # Handle the case where sending might fail due to a disconnected client
                        pass

    # Remove the client that has disconnected
    with lock:
        sockets.remove(connection)
        connection.socket.close()

serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print(f"The server is ready to receive on port {serverPort}")

sockets = []
lock = threading.Lock()

while True:
    connectionSocket, addr = serverSocket.accept()
    connection = Connection(connectionSocket, addr)
    with lock:
        sockets.append(connection)
    print(f"Connection received from {addr}")

    # We create a new thread for each client connection. The `target`
    # argument is the function that will be executed inside the new thread.
    # The `args` argument is the arguments to pass to the function.
    # So in this case, we're creating a new thread and running `handle_client(connection))`
    # inside of it.
    # After creating the new thread object, we have to actually tell Python to *start*
    # the thread. Then that thread starts to run in parallel with this thread.
    client_thread = threading.Thread(target=handle_client, args=(connection,))
    client_thread.start()
