# Week 3: Application Layer (Other than HTTP)

## Learning Goals

* I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.
* I can explain the role of each DNS record type.
* I can explain the role of the SMTP, IMAP, and POP protocols in the email system.
* I know what each of the following tools are used for: `nslookup`, `dig`, `whois`.

## Readings and Resources

* *Kurose*, Sections 2.3, 2.4, 2.5, 2.6

## Eat Your Vegetables

Eat your vegetables using the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

This week's questions are from Chapter 2.
* From the Review Questions: R16, R18, R24
* From the Problems section: P16, P18(a and b only), P20, P21

## Grow More Vegetables

Contribute some self-assessment questions to the class's question bank. This can include writing new questions or editing/improving existing ones. ([Our Class Bulletin Board has the login information.](https://docs.google.com/document/d/1M0ZK6oFZVgWBMS4ZgqY_FCec8o2WSLQ7LBkrCxC2IXE/edit?usp=sharing))

## Community Chest

Do at least one thing to improve the physical or digital space of the class.

### Community Chest

Do at least one thing to make the digital or physical space of the classroom better for everyone.

## Labs

Choose **one** and add it to your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).

* **Explore DNS with Wireshark**. Complete the textbook DNS Wireshark Lab [on this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php). Submit the answers in your Portfolio.
* [**Register a Domain Name**](/assignments/week03/register-a-domain-name.md)
* Complete a "Deep Dive" in accordance with the [Deep Dive standards](/standards/README.md#deep-dives).

## 📓 Portfolio Submission

This week your Portfolio should include:

1. A Lab writeup of your choice, in accordance with the [Lab Standards](/standards/README.md#lab-assignments) or [Deep Dive Standards](/standards/README.md#deep-dives).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).
