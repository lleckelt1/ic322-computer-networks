# Making Something: Register a Domain Name

This activity asks you to:
1. Register a domain name
2. Configure DNS so that 

Registering a domain name costs money (though not as much as you might think). There are other activities this week available that do not cost any money.

This activity will not hold your hand! You will be reading through documentation and possibly online tutorials. You have lots of resources: your classmates, your instructor, artificial superintelligences, and the entire internet. 

But the hard work will pay off. After you're done this activity you will have your own website with its own domain name accessible to every human on the planet. You've written "Hello World" so many times already... but this time you'll actually be heard.

## Step 0: Make a Website

There's no point in having a domain name if you don't have a server waiting to receive traffic. Here are two common ways to host websites. Choose one of them.

1. **Host a website on Gitlab**. Gitlab has a feature called "Gitlab Pages" that allows you to host a "static" website for free. "Static" in this sense means that objects (files) are served directly from the file system, they're not dynamically created with software. Use [Gitlab's Pages documentation](https://docs.gitlab.com/ee/user/project/pages/) to set up a static site in one of your repositories. You can even use your Portfolio repository and show off your networking knowledge to the world! (Step 2 below has more information about setting up Gitlab with a custom domain.)
2. **Set up a server with a dedicated IP address**. This is how most websites work. However, this also requires a bit of know-how. The computers connected to the USNA network are part of a private LAN; you can't access them from outside the campus. Instead, you need to find a computer connected directly to the internet. Most people use virtual machines in the cloud to run their servers. AWS is the biggest provider. This lab won't walk you through the steps to set up your own server, but you can [find plenty of tutorials online](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html). AWS even has a "free tier" that gives you a bit of server space for free for 12 months.

## Step 1: Register a Domain Name

Find an online domain registrar and buy a domain name. [Namecheap](https://www.namecheap.com/) and [Squarespace](https://www.squarespace.com/) both sell domain names.

For this exercise, there's no need to sign up for additional services like SSL (TLS), or "Premium DNS". What we want is to be assigned a domain name, and to configure the DNS records for that domain name.

**Question 1**: You should probably sign up for "Domain Privacy". What is this, and why should you sign up for it?

## Step 2: Set up your DNS Records

After registering a domain name, set up the DNS records. The interface for each registrar is different, but you're looking for a settings panel that allows you to set `A` records, `CNAME` records, and the like. For Squarespace this is the default. If you're using Namecheap, you'll need to go to "Advanced DNS".

Once you're able to edit the DNS settings, set up DNS records so that visiting your domain name results in seeing your website.

**Question 2**: If you have a server with an IP address of `77.97.5.74` and a domain name `ic322.com`, what DNS record would you use to configure your website?

**Question 3**: If you have a website that is already accessible at `m091890.gitlab.io/ic322-computer-networks`, and you registered a domain `ic322.com`, what DNS record would you use to configure your website?

**Here are some tips if you're using Gitlab Pages.**

1. Part of the instructions liked above were setting up a "custom domain". [You can access those instructions directly here](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html).
2. You will need to create a `.gitlab-ci.yml` file in order to set up your website. This is a "continuous integration" configuration file. Many software projects use CI to build and test their software every time they push changes to their code base. Gitlab Pages uses it to "build" your website. [This website's `.gitlab-ci.yml`](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/blob/main/.gitlab-ci.yml?ref_type=heads) just copies the entire repository into a `public` folder. You can copy that configuration file into your repository.
3. By default, servers serve the `index.html` to connecting clients. You'll need one. This site uses [Docsify](https://docsify.js.org/#/) to dynamically convert markdown files to html. You can copy [that `index.html` file](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/blob/main/index.html?ref_type=heads) to your own repository. Or you can create your own `index.html` file that says whatever you want!

**Question 4**: The Gitlab Pages instructions use a `TXT` record to verify ownership of the domain name. The book does not cover `TXT` records. What are they?

## Step 3: Visit Your Website

Now that you've registered a domain name and set up your DNS records, visit your website using your new domain name!

**Question 5**: What were the DNS records you set up?

**Question 6**: Who owns the following DNS servers for your website?
* Root
* TLD
* Authoritative

**Question 7**: If you register a domain like `gonavy.com`, and have a mail server running at IP `34.234.11.05`, what DNS records would you need to create in order for mail addressed to `bill@beatarmy.com` to be delivered to your mail server?

## Stretch Goal: Set Up Email

You probably remember from the book that `MX` records tell the DNS system how to reach the mail server associated with the domain. Now that you have a domain name, imagine being able to tell people your email is `johan@thor.pizza`. If you own the `thor.pizza` domain, *any* email sent to `@thor.pizza` will end up at your mail server with a correctly configured `MX` record. (Classmates need help? Send them to `customercare@thor.pizza`. Mom wants to visit? `scheduling@thor.pizza`. Just kidding, mom gets `vipservices@thor.pizza`) You could even rent out email addresses and start your own email service.

Most registrars offer some kind of email service for your domain names, and they usually set up the `MX` records for you. I use a 3rd party email service called [Fastmail](fastmail.com) and set up my own `MX` records.

## Submission

Include this activity in your Portfolio in accordance with the [Lab Standards](/standards/README.md#lab-assignments). Don't forget to include a link to your website!
