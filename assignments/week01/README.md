# Week 1: The Internet

## Learning Goals

This week we will be introducing the biggest computer network of all - the Internet! We will be learning foundational concepts that we will use for the rest of the semester.

Here are the learning objectives for this week:

* I can explain the role that the network core plays vs the network edge.
* I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.
* I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.
* I can describe the differences between packet-switched networks and circuit-switched networks.
* I can describe how to create multiple channels in a single medium using FDM and TDM.
* I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact. 
* I can explain how encapsulation is used to implement the layered model of the Internet.

## Reading and Resources

* *Kurose*, Chapter 1
* [End-to-end packet delay walkthrough available in the Community Materials](https://drive.google.com/drive/folders/14NsJpysE1Kjyds0l7IPRyzBGr8Kr5P7e?usp=sharing).

## Eat Your Vegetables

The standards for this activity can be found in the [Standards document](/standards/README.md#eat-your-vegetables).

1. Answer the following review questions from the end of the chapter: R1, R4, R11, R12, R13, R14, R18, R19. Include the questions and answers in your Portfolio.

2. Answer each of the Learning Goals and include the answers in your Portfolio.

## Grow More Vegetables

Contribute some questions to the class's Quizziz question bank. We use a shared login to contribute questions for every week. The login information is kept on our [Bulletin Board](https://docs.google.com/document/d/1M0ZK6oFZVgWBMS4ZgqY_FCec8o2WSLQ7LBkrCxC2IXE/edit?usp=sharing).

Once you login at [quizizz.com](https://quizizz.com), you can find this week's question bank by:

1. Clicking "My Library" on the left menu
2. Clicking "Drafts" at the top of the screen.
3. Click this week's topic.
4. Click "Continue Editing"

<figure>
    <img src="/assignments/week01/figs/quizziz-logging-in.png" alt="Quizizz Drafts" width="300" />
</figure>


More information can be found in the [Standards document](/standards/README.md#grow-more-vegetables), including a link to the log in information.

### Community Chest

Do at least one thing to make the physical space of the classroom better for everyone.

## Wireshark Introduction


Two of the most important, foundational concepts we're learning this week are *packet switching* and *encapsulation*. Wireshark is a computer program called a *packet sniffer*. This is a program that allows you to capture and inspect all the packets leaving from and arriving at your computer. With the proper configuration, a packet sniffer can even see *all* the traffic on a network (even traffic intended for other computers!).

This week you'll install and become familiar with Wireshark. The textbook has a good introduction.

* Complete the "Getting Started" Wireshark assignment [available at the textbook website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php). Use the latest version of the instructions (8.1).

Some notes:

* You should be able to complete the lab using your school computer or a lab computer. You won't need to use a "trace file" (or a pcap file) mentioned in the lab. These are files that save network traffic captures and are sometimes used to standardize tutorials. This assignment should feel a little bit like stepping into a busy city: you'll see thousands of packets flying through the network every minute. Welcome to the Internet! One goal of this assignment will be learning how to filter those packets down to find exactly what you're looking for.
* Anyways, since we're not using a trace file, references to specific packet numbers (like the "hint: this is packet number 286") will not apply.

Lab standards can be found in the [Standards document](/standards/README.md#lab-assignments).

## Portfolio Submission

Include the following in your Portfolio:

1. The Wireshark Into Lab, in accordance with the [Lab Standards](/standards/README.md#lab-style-assignments).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

I have an example Portfolio with a few weeks of assignments. [You can see it here](https://gitlab.com/jldowns-usna/ic322-downs-portfolio). This might come in handy if you're trying to troubleshoot something with your Portfolio. It's also a good example of what I would call a "pretty good" Portfolio in case you're trying to calibrate your standards.