# Week 5: Transport Layer

## Learning Goals

* I can explain the problem that TCP congestion control is solving for, and how it solves that problem.
* I can explain the problem that TCP flow control is solving for, and how it solves that problem.

## Readings and Resources

* *Kurose*, Sections 3.5.5 - 3.9

## Eat Your Vegetables

Eat your vegetables using the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

This week's questions are from Chapter 3.
* From the Review Questions: R17, R18, 
* From the Problems section: P27, P33, P36, P40

## Grow More Vegetables

Contribute some self-assessment questions to the class's question bank. This can include writing new questions or editing/improving existing ones.

## Community Chest

Do at least one thing to improve the physical or digital space of the class.

## Labs

* *TBD* Stand by.

## 📓 Portfolio Submission

This week your Portfolio should include:

1. A Lab writeup of your choice, in accordance with the [Lab Standards](/standards/README.md#lab-assignments) or [Deep Dive Standards](/standards/README.md#deep-dives).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).