# Week 0: Welcome to IC322

## Learning Goals

* I can create a repository on Gitlab and push and pull changes from the remote server.
* I can clone an existing repository
* I can use markdown to write documents with:
    * headings and body text, including styling text (bold and italics)
    * relative links to files in the same repository
    * absolute links to files on the Internet
    * images located in the same repository

## Reading and Resources

Make sure you have the required textbook: *[Computer Networking: A Top-Down Approach, 8th edition](https://gaia.cs.umass.edu/kurose_ross/index.php)*. We will start with Chapter 1 [next week](/assignments/week01/README.md).

## Create Your Portfolio

In this course, all assignments are submitted in a Portfolio that is due every Monday (or Tuesday if Monday is a holiday.) You will be maintaining this portfolio all semester long. Be proud of your Portfolio! Make it nice to use!

Concretely, your Portfolio will exist as a git repository on Gitlab. Your first assignment of the semester is to create your Portfolio. I've split the task into many steps, but each step is pretty simple.

1. Create a Gitlab account
2. Create an empty repository on Gitlab
3. Create code in your local repository
4. Create a local repository
5. Commit the code in your local repository
6. Link your local repository to your Gitlab (empty) repository.
7. Push your changes from your local repository to the remote repository.
8. Let your instructor know your Portfolio's URL

If you're proficient with `git` and Gitlab already, you can scan the walkthrough and just create the repository with the `README.md` file. There aren't any gotchas or non-standard settings.

[Here is the guide that will walk through each of the steps above](/assignments/week00/create-your-portfolio.md).

I have an example Portfolio with a few weeks of assignments. [You can see it here](https://gitlab.com/jldowns-usna/ic322-downs-portfolio). This might come in handy if you're trying to troubleshoot something with your Portfolio. It's also a good example of what I would call a "pretty good" Portfolio in case you're trying to calibrate your standards.



## Portfolio Submission

The last step of the assignment is to let your instructor know the URL for your portfolio. You do this by updating the Google Sheet titled "IC322 AY23 Portfolio URLs" in our [Community Materials Drive](https://drive.google.com/drive/folders/1OsXHeg7SPHC1k5SgDYtfE9fzGsmNwlB7?usp=sharing).

