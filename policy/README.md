# IC322 Computer Networks - Course Policy

**Coordinator**: LCDR Justin Downs, HP 451, x3-6806, downs@usna.edu  
**Credits**: 2-2-3  
**Pre-requisites**: IC221 or SY204  
**Co-requisite**: SM242 or equivalent.

In IC322, we dive into the basics of computer communications and networks, exploring their core ideas and features. We'll discuss how these elements impact network performance and design. We focus on the protocols that power the Internet, including mobile networks and network security. 


## Learning Objectives

*These objectives are linked to the [USNA Computer Science Student Outcomes](https://usna.edu/CS/Assessment/CSStudentOutcomes.php).*

1. Evaluate the operation and performance of practical computer-network protocols for communications. *(Student Outcome 1)*
1. Understand the fundamentals of network communications include routing, congestion control techniques, internet-working, addressing, connection establishment, and reliable transport. *(Student Outcome 1)*
1. Understand and develop approaches and techniques for communicating across a network. *(Student Outcomes 2 and 3)*
1. Use modern tools to analyze network traffic. *(Student Outcome 1)*
1. Understand the legal and moral issues associated with network communications and the world-wide effects of illicit hacking. *(Student Outcome 4, Organizations)*
1. Evaluate the level of security of a network, and understand how to further secure it. (Student Outcomes 1 and 2)
1. Understand the principles of wireless networks and underlying electromagnetic spectrum technology. *(Student Outcome 1)*


## Textbook

We will be using Kurose's *[Computer Networking: A Top-Down Approach, 8th edition](https://gaia.cs.umass.edu/kurose_ross/index.php)*. This textbook is required.

## Collaboration and Plagiarism

You should spend the majority of class time collaborating with your classmates on the assignments. You should also collaborate on assignments outside of class! 

Plagiarism is turning in work that is not your own. This can be complicated, since "work" means a lot of things. Copying, word for word, the work of someone else and claiming it as your own is plagiarism. Being inspired by someone else's work probably isn't.

🤖 This semester you will have access to AI Chatbots (or "LLMs" as we say in the biz). The same rules apply to LLMs as to humans, with one addition. Asking an LLM about networking topics is encouraged! Asking an LLM to help troubleshoot your code is also encouraged! LLMs might also be able to help edit your writing. Copying the output of an LLM and claiming it as your own work is plagiarism. Here's the addition: if you use an LLM for this class, always keep a copy of the conversation and include it in your Portfolio. (See my [Portfolio](https://gitlab.com/jldowns-usna/ic322-downs-portfolio/-/blob/main/week03/domain-chatgpt.md) for examples.) I've also included LLM guidance for every type of assignment in the [course standards](/standards/).

Keep in mind the goal of the course: to develop a broad understanding of computer network topics and mastering networking skills expected of a computer scientist. If too much of your work is derived from someone else's work and you haven't cultivated your own mastery, you won't have met the course goal.

Some in-class activities, like Knowledge Checks and Exams, are to be done by yourself. But it will be clear when collaboration is prohibited. If you're unsure, just ask.

## Grading

All work (besides exams and knowledge checks) will be submitted in a Portfolio. We will cover the specific requirements for the Portfolio during the first week of class.

**Weekly Feedback**: I'll provide feedback on your Portfolio every week. I will provide feedback on the last week's assignment as well as (optionally) an update to one earlier assignment.

**Revising Work**: You can update work in your Portfolio all semester, and only the most recent update will be graded. I can provide feedback on updated assignments.

**Repeating Knowledge Checks**: You will have one opportunity every week to take Knowledge Checks. You can take as many as you like, including past Knowledge Checks.

**Grading**: Your Portfolio will be graded three times: at 6 weeks, 12 weeks, and for a final grade. Again, only the most recent updates will be graded.

**Late Policy**: Weekly assignments are due in your Portfolio the Monday after they are assigned. There is no "grade penalty" for late work, but I won't be able to provide feedback for assignments you haven't completed.

**Letter Grades**: Your 6 week, 12 week, and final grades will be assigned as letter grades. Your 6 week, 12 week, and final exam grades will be used to determine the plus/minus.

| Exam Grade | Grade Adjustment       |
| ---------- | ---------------------- |
| 85% - 100% | +                      |
| 70-84%     | No adjustment          |
| 50-69%     | -                      |
| < 50%      | Next letter grade down |

## Classroom Conduct

The section leader will record attendance and bring the class to attention at 
the beginning and end of each class. If the instructor is more than 5 minutes late, the section leader will keep the class in place and report to the Computer Science department office. If the instructor is absent, the section leader will direct the class. Drinks are permitted, but they must be in reclosable containers. Food, alcohol, smoking, smokeless tobacco products, and electronic cigarettes are all prohibited. Cell phones must be silent during class. All discussions will be civil, and both faculty and midshipmen will be treated with dignity and respect at all times. 

## Remote Classes
By default, all classes require in-person attendance. If the course switches to hybrid- or remote-style classes, the instructor will communicate that explicitly.  Remote classes may be recorded for future reference. Remote attendees will make every effort to connect to class sessions and give them undivided attention. Remote attendees will adhere to the same uniform and grooming standards as those attending in person. 

## Schedule

**[Week 0: Getting Ready](/assignments/week00/)**  
17 Aug (Monday Schedule)  
18 Aug  

**[Week 1: The Internet](/assignments/week01/)**  
*Structure of the Internet, delay, throughput, layers, and encapsulation*  
21 Aug  
24 Aug  
*~~25 Aug: No Class~~*  

**[Week 2: Application Layer: HTTP](/assignments/week02/)**  
*HTTP, email protocols, DNS, and CDNs.*  
28 Aug  
31 Aug  
1 Sep  

**[Week 3: Application Layer: DNS](/assignments/week03/)**  
*HTTP, email protocols, DNS, and CDNs.*  
*~~4 Sep: No Class (Labor Day)~~*  
7 Sep  
8 Sep (Welcome, parents!)  

**[Week 4: Transport Layer](/assignments/week04/)**  
*TCP, UDP, congestion control, flow control, and SSL.*  
11 Sep  
14 Sep  
15 Sep  

**[Week 5: Transport Layer](/assignments/week05/)**  
*TCP, UDP, congestion control, flow control, and SSL.*  
18 Sep  
21 Sep  
22 Sep  

**[📓 Week 6: Six Week Assessment](/assignments/week06/)**  
25 Sep  
28 Sep  
29 Sep  

**[Week 7: Transport Layer Security](/assignments/week07/)**  
*TLS, Key Exchange, CAs, Authenticity, Integrity, and Confidentiality.*  
2 Oct  
5 Oct  
6 Oct  

**[Week 8: Network Layer: Data Plane](/assignments/week08/)**  
*Routers and hardware, IP, CIDR, subnets, DHCP, NAT, and ICMP.*  
*~~9 Oct: No Class~~*  
10 Oct (Friday Schedule)  
12 Oct  
13 Oct  

**[Week 9: Network Layer: Control Plane](/assignments/week09/)**  
*LS and DV routing, RIP, OSPF, and BGP.*  
16 Oct  
19 Oct  
20 Oct  

**[Week 10: Link Layer](/assignments/week10/)**  
*MAC addressing, ARP, Ethernet, switches and hardware, VLANs, ALOHA, CSMA/CD, CRC and error detection and correction.*  
23 Oct  
26 Oct  
27 Oct  

**[📓 Week 11: Twelve Week Assessment](/assignments/week11/)**  
30 Oct  
2 Nov  
3 Nov  

**[Week 12: Wireless and Mobile Networks](/assignments/week12/)**  
*802.11 and cellular internet access.*  
6 Nov  
9 Nov  
*~~10 Nov: No Class (Veterans Day)~~*  

**[Week 13: Wireless and Mobile Networks](/assignments/week13/)**  
*802.11 and cellular internet access.*  
13 Nov  
16 Nov  
17 Nov  

**[Week 14: Security in Computer Networks](/assignments/week14/)**  
20 Nov  
22 Nov (Friday Schedule)  
*~~23 Nov: No School (Thanksgiving)~~*  
*~~24 Nov: No School (Thanksgiving)~~*  

**[Week 15: Security in Computer Networks](/assignments/week15/)**  
27 Nov  
30 Nov  
1 Dec  

**[📓 Week 16: Final Assessment](/assignments/week16/)**  
4 Dec  
7 Dec  
