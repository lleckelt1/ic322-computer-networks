# Python Quick-start

## Creating a Python program

Python programs use the `.py` extension. Here is an example:

```python
# hello.py

print("Hello, world!")
```

There's not a lot of boilerplate required to get a program running - no `int main()`, no required classes, no required `return 0`. It's typical to just start writing code at the top level of the file.

To run this program, open a terminal and type `python3 hello.py`. You should see the following output:

```
Hello, world!
```

## Variables

Python is a dynamically typed language. This means that you don't need to declare variables before using them. You can just assign a value to a variable and start using it.

```python
score = 0
score = score + 1
print(score) # 1

name = "John"
print(name) # John

name = 5
print(name) # 5
```

## Comments

Python uses the `#` character to indicate a comment.

```python
# This is a comment
print("Hello, world!") # This is also a comment
```

Python does not have multi-line comments like C++ or Java.

```python
# We use multiple single-line comments to
# create a multi-line
# comment.
```


## Lists

Python has a built-in data structure called a "list". Lists are similar to arrays in other languages. They can contain any type of data, and they can grow and shrink in size.

```python
scores = [200, 189, 203, 198, 182]

# Adding an element
scores.append(199)
scores = scores + [201]

# Removing an element
scores.pop(0)

# Accessing an element
print(scores[0]) # 189
print(scores[1]) # 203

# You can use negative indices to access elements from the end of the list!
print(scores[-1]) # 201
print(scores[-2]) # 199

# You can also select a range of elements using the colon operator
print(scores[1:3]) # [203, 198]

# You can omit the start or end of the range to select all elements up to or from the start or end of the list
print(scores[:3]) # [189, 203, 198]
print(scores[3:]) # [198, 182, 199, 201]
```



## Strings

Strings can be created with single or double quotes.

```python
name = "John"
name = 'John'
```

There are multiline strings, too.

```python
haiku = """
Packets flow like streams,
Silent whispers in the wire,
Web of dreams unfolds.
"""
```

Python has a useful feature called "string interpolation". This allows you to insert variables into strings. We call these "f-strings" because they start with the letter `f`.

```python
name = input("What is your name? ")
print(f"Hello, {name}!") # Hello, John!

# Multiline strings work, too.
web_material = "dreams"
haiku = f"""
Packets flow like streams,
Silent whispers in the wire,
Web of {web_material} unfolds.
"""
print(haiku)
```

You can select portions of strings using the same syntax as lists.

```python
name = "John"
print(name[0]) # J
print(name[1]) # o
print(name[2:]) # hn
print(name[:2]) # Jo
print(name[-1]) # n
```

## Conditionals and Loops

Python uses indentation to indicate blocks of code. This is similar to how curly braces are used in other languages.

```python
if 5 > 3:
    print("5 is greater than 3")
else:
    print("5 is not greater than 3")
```

Python's `for` loop is similar to the `foreach` loop in other languages.

```python
scores = [200, 189, 203, 198, 182]
for score in scores:
    print(score)
```

If you want to loop over a range of numbers, you can use the `range` function. This feels similar to a `for` loop in other languages, but what it actually does is create a list of numbers and then loops over that list.

```python
for i in range(5):
    print(i) # 0, 1, 2, 3, 4
```

But it's best to use the `for` loop with lists directly. If you need to also know what index you're on, you can use the `enumerate` function.

```python
scores = [200, 189, 203, 198, 182]
for i, score in enumerate(scores):
    print(f"Score {i}: {score}")
```

# Functions

Functions are defined using the `def` keyword. You call them like you would in other languages.

```python
def add(a, b):
    return a + b

print(add(5, 3)) # 8
```

# Classes

Like we covered before, you don't have to wrap everything in a class like you do in Java. And you don't have to wrap everything in a function like you do in C. In fact, for most simple Python programs, you don't need to define your own classes at all. *But* if you create more complicated programs, classes come in handy. So there are classes in Python.

```python
class Person:
    def __init__(self, name):
        self.name = name

    def greet(self):
        print(f"Hello, {self.name}!")

friend = Person("John")
friend.greet() # Hello, John!
```

The `__init__` function is the constructor. Python doesn't have private or protected methods (to keep it simple!) so it has become convention to prefix functions you don't want users to touch with one or two underscores (of course!) The constructor definition `__init__` is an example where convention became official: it's the actual way to define a constructor in Python.

You might also note that there is a `self` parameter in every method. It's similar to the `this` keyword in other languages. `self` needs to be the first argument of every instance method definition. Confusingly, you *don't* use it when calling the method; it's included automatically. So, in the example above, we call `friend.greet()` without passing in a `self` parameter.

There are [static and dynamic methods](https://www.geeksforgeeks.org/class-method-vs-static-method-python/) and [data classes](https://realpython.com/python-data-classes/), but if you're getting to that level in this class you can look those up for yourself.

# Modules

Python was one of the first languages to have a robust package manager. It's called `pip`. You can use it to install packages from the [Python Package Index](https://pypi.org/). For example, if you want to install the [requests](https://pypi.org/project/requests/) package, you can run `pip install requests` in your terminal.

```python
import requests

response = requests.get("https://www.google.com")
print(response.status_code) # 200
print(response.text) # <!doctype html>...
```

---

And that's it! There are a lot of other features, but you can learn them as you go. The [Python documentation](https://docs.python.org/3/) is a great resource if you want to learn more.
```