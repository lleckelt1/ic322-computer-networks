# IC322: Computer Networks

<img src="le_computers.png" width="800">

- [🪑 Schedule a Meeting](https://calendar.app.google/MrVi9qk7TWpKnAvJ6)
- [📎 Course Policy](/policy/)
- [📏 Assignment Standards](/standards/)
- [🗓️ Class Schedule](/policy/README.md#schedule)
- [📓 Assignments](/assignments/)
- [🗂️ References](/references/)
- [🪴 Community Material](https://drive.google.com/drive/folders/1OsXHeg7SPHC1k5SgDYtfE9fzGsmNwlB7?usp=sharing)

**MGSP is held Sundays and Tuesdays at 2100 in CH117.**

In IC322 we dive into the basics of computer communications and networks, exploring their core ideas and features. We'll discuss how these elements impact network performance and design. We focus on the protocols that power the Internet, including mobile networks and network security...

This site is available in several places:
* On the internal network at [courses.cs.usna.edu](https://courses.cs.usna.edu/).
* The source code is kept on GitLab at [gitlab.com/jldowns-usna/ic322-computer-networks](https://gitlab.com/jldowns-usna/ic322-computer-networks).
* A an up-to-date render of the site is kept on GitLab Pages, [here](https://jldowns-usna.gitlab.io/ic322-computer-networks/#/).
* It's also mirrored at [ic322.com](http://ic322.com/)!